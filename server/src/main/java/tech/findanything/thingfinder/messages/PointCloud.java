package tech.findanything.thingfinder.messages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class PointCloud {

	private Transform center;
	private Transform extents;
	private ArrayList<Transform> points;
	
	public Transform getCenter() {
		return center;
	}
	
	public Transform getExtents() {
		return extents;
	}
	
	public ArrayList<Transform> getPoints() {
		return points;
	}

	public PointCloud(Transform center, Transform extents, ArrayList<Transform> points) {
		super();
		this.center = center;
		this.extents = extents;
		this.points = points;
	}

	public PointCloud() {

		super();
		
		Transform t1 = new Transform(1, 1, 1);
		Transform t2 = new Transform(1, 1, 2);
		Transform t3 = new Transform(1, 2, 1);
		Transform t4 = new Transform(1, 2, 2);
		Transform t5 = new Transform(2, 1, 1);
		Transform t6 = new Transform(2, 1, 2);
		Transform t7 = new Transform(2, 2, 1);
		Transform t8 = new Transform(2, 2, 2);
		
		points = new ArrayList<>(Arrays.asList(t1, t2, t3, t4, t5, t6, t7, t8));
		
//		System.out.println("Generating 5000 points");
//		
//		Random rand = new Random();
//		for (int i = 0; i < 5000; i++) {
//			points.add(new Transform(rand.nextDouble() * 100, rand.nextDouble() * 100, rand.nextDouble() * 100));
//		}
		
	}

	@Override
	public String toString() {
		return "PointCloud [center=" + center + ", extents=" + extents + ", points=" + points + "]";
	}
	
}
