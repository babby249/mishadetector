package tech.findanything.thingfinder.messages;

import java.util.ArrayList;

public class DetectionFrame {

	private ArrayList<Detection> detections;
	
	/**
	 * As base 64
	 */
	private String image;

	public ArrayList<Detection> getDetections() {
		return detections;
	}

	public String getImage() {
		return image;
	}

	@Override
	public String toString() {
		return "DetectionFrame [detections=" + detections + ", image=" + image + "]";
	}
	
}
