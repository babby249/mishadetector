package tech.findanything.thingfinder.messages;

public class BoundingBox {

	private double x;
	private double y;
	private double height;
	private double width;

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getHeight() {
		return height;
	}

	public double getWidth() {
		return width;
	}

	@Override
	public String toString() {
		return "BoundingBox [x=" + x + ", y=" + y + ", height=" + height + ", width=" + width + "]";
	}
	
}
