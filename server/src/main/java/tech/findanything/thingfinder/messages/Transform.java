package tech.findanything.thingfinder.messages;

public class Transform {

	private double x;
	private double y;
	private double z;
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}

	public Transform() {
		super();
	}

	public Transform(double x, double y, double z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public String toString() {
		return "Transform [x=" + x + ", y=" + y + ", z=" + z + "]";
	}
	
	public double distanceTo(Transform other) {
		
		double xDis = getX() - other.getX();
		double yDis = getY() - other.getY();
		double zDis = getZ() - other.getZ();
		
		return Math.sqrt(Math.pow(xDis, 2) + Math.pow(yDis, 2) + Math.pow(zDis, 2)); 
		
	}
	
	public boolean isDistanceToUnder(Transform other, double maxDistance) {
		
		double distance = distanceTo(other);
		return distance < maxDistance;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transform other = (Transform) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}
	
}
