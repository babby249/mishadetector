package tech.findanything.thingfinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tech.findanything.thingfinder.data.DataService;
import tech.findanything.thingfinder.messages.Detection;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class RestController {

	@Autowired
	private DataService dataService;
	
    @RequestMapping(value = "/detection/random/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getRandomDetectionImage() throws IOException {
    	Random rand = new Random();
    	ArrayList<Detection> arr = new ArrayList<>(dataService.getDetections());
    	UUID id = arr.get(rand.nextInt(arr.size())).getId();
    	return dataService.getImageOfDetection(id);
    }
	
    @RequestMapping(value = "/detection/{id}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getDetectionImage(@PathVariable("id") UUID id) throws IOException {
    	return dataService.getImageOfDetection(id);
    }
	
    @RequestMapping(value = "/detections")
    public String getDetections() {
    	return dataService.getDetections().toString();
    }
	
    @RequestMapping( method = RequestMethod.POST,  value = "/reset/xddd")
    public String reset() {
    	dataService.reset();
    	return "OI the server is now reset, I hope you are happy";
    }
	
}
