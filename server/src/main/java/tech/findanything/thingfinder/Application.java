package tech.findanything.thingfinder;

import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

import tech.findanything.thingfinder.data.DataService;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Bean
	public DataService dataService() {
		return new DataService();
	}
	
    @Bean
    public ServletServerContainerFactoryBean createServletServerContainerFactoryBean() {
        log.info("createServletServerContainerFactoryBean: Websocket factory returned");
        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        container.setMaxTextMessageBufferSize(1024 * 1024 * 10);
        container.setMaxBinaryMessageBufferSize(1024 * 1024 * 10);
        return container;
    }
    
    private static Random rand = new Random();
    
    public static UUID getUUID() {
    	return new UUID(rand.nextLong(), rand.nextLong());
    }
    
	static long start;
	static long end;
	
	public static void startTimer() {
		start = System.currentTimeMillis();
	}

	public static void endTimer() {
		endTimer("That");
	}
	
	public static void endTimer(String action) {
		end = System.currentTimeMillis();
		log.info("{} took {} millis", action, (end - start));
	}
    
}
