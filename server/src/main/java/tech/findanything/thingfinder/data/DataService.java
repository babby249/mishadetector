package tech.findanything.thingfinder.data;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tech.findanything.thingfinder.Application;
import tech.findanything.thingfinder.messages.BoundingBox;
import tech.findanything.thingfinder.messages.CameraTransform;
import tech.findanything.thingfinder.messages.Detection;
import tech.findanything.thingfinder.messages.DetectionFrame;
import tech.findanything.thingfinder.messages.PointCloud;

public class DataService {

	private static final Logger log = LoggerFactory.getLogger(DataService.class);
	
	/**
	 *  The last point cloud that was sent
	 */
//	private PointCloud previousPoints;
	
	/**
	 *  The point cloud that was most recently received
	 */
	private PointCloud points = new PointCloud();
	private Set<Detection> detections = ConcurrentHashMap.newKeySet();
	private CameraTransform cameraTransform = new CameraTransform();
	private HashMap<UUID, UUID> detectionToImage = new HashMap<>();
	private HashMap<UUID, byte[]> images = new HashMap<>();
	
	public PointCloud getPoints() {
		return points;
	}

	public CameraTransform getCameraTransform() {
		return cameraTransform;
	}

	public Set<Detection> getDetections() {
		return detections;
	}

	public void receivedPoints(PointCloud newPoints) {
		this.points = newPoints;
	}

	public void receivedCameraTransform(CameraTransform cameraTransform) {
		this.cameraTransform = cameraTransform;
	}
	
	final double MAX_DISTANCE = 2;
	
	public void receivedDetectionFrame(DetectionFrame detectionFrame) {
		
		long start = System.currentTimeMillis();
		
		log.info("Processing {} detections", detectionFrame.getDetections().size());
		
		UUID imageId = Application.getUUID();
		boolean anyUpdated = false;
		
		for (Detection detection : detectionFrame.getDetections()) {

			long nearbyDetections = detections.stream()
					.filter(d -> d.getClassName().equals(detection.getClassName()))
					.filter(d -> d.getTransform().isDistanceToUnder(detection.getTransform(), MAX_DISTANCE))
					.count();
			
			if(nearbyDetections >= 1) {
				log.info("Ignoring detection {}", detection.getClassName());
				continue;
			} else {
				log.info("Accepting detection {}", detection.getClassName());
			}
			
			anyUpdated = true;
			UUID detectionId = detection.setId();
			detections.add(detection);
			detectionToImage.put(detectionId, imageId);
		}
		
		if(anyUpdated) {
			byte[] image = Base64.getMimeDecoder().decode(detectionFrame.getImage());
			images.put(imageId, image);
		} else {
			log.info("No detections accepted - discarding frame");
		}
		
		long end = System.currentTimeMillis();
		log.info("Total detections now {}. Processing took {} ms", detections.size(), (end-start));
		
	}
	
	/**
	 * Returns byte64 encoded image that belongs to this detection
	 * @throws IOException 
	 */
	public byte[] getImageOfDetection(UUID detectionId) throws IOException {
		
		Detection detection = detections.stream().filter(d -> d.getId().equals(detectionId)).findAny().get();
		UUID imageId = detectionToImage.get(detectionId);
		byte[] image = images.get(imageId);
		
		BoundingBox box = detection.getBoundingBox();
		
		int xDelta = (int) (box.getWidth() * 1440);
		int yDelta = (int) (box.getHeight() * 1440);
		
		int x = (int) (box.getX() * 1440);
		int y = (int) ((box.getY() * 1440) + ((1920 - 1440) / 2));
				
		InputStream in = new ByteArrayInputStream(image);
		BufferedImage bImage = ImageIO.read(in);

	    int w = bImage.getWidth();
	    int h = bImage.getHeight();
	    double theta = Math.PI / 2;
	    
	    AffineTransform xform = new AffineTransform();
	    xform.translate(0.5*h, 0.5*w);
	    xform.rotate(theta);
	    xform.translate(-0.5*w, -0.5*h);
	    
	    BufferedImage rot = new BufferedImage(h, w, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = rot.createGraphics();
		
		g2d.drawImage(bImage, xform, null);
		g2d.setColor(new Color(104, 159, 56));
		g2d.setStroke(new BasicStroke(4));
		g2d.setFont(new Font(null, -190, 48));
		g2d.drawRect(x, y, xDelta, yDelta);
//		g2d.drawString(detection.getClassName(), 50, 50);
		g2d.dispose();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(rot, "jpg", baos);
		byte[] imageBoxed = baos.toByteArray();

		return imageBoxed;

	}
	
	public void reset() {
		points = new PointCloud();
		detections = ConcurrentHashMap.newKeySet();
		cameraTransform = new CameraTransform();
		detectionToImage = new HashMap<>();
		images = new HashMap<>();
	}
	
}
