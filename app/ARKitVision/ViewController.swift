/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Main view controller for the ARKitVision sample.
*/

import UIKit
import SpriteKit
import ARKit
import Vision
import VideoToolbox
import StompClientLib


extension UIImage {
    public convenience init?(pixelBuffer: CVPixelBuffer) {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(pixelBuffer, options: nil, imageOut: &cgImage)
        
        if let cgImage = cgImage {
            self.init(cgImage: cgImage)
        } else {
            return nil
        }
    }
}


class ViewController: UIViewController, UIGestureRecognizerDelegate, ARSKViewDelegate, ARSessionDelegate, StompClientLibDelegate {
    
    @IBOutlet weak var sceneView: ARSKView!
    
    // The view controller that displays the status and "restart experience" UI.
    private lazy var statusViewController: StatusViewController = {
        return children.lazy.compactMap({ $0 as? StatusViewController }).first!
    }()
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure and present the SpriteKit scene that draws overlay content.
        let overlayScene = SKScene()
        overlayScene.scaleMode = .aspectFill
        sceneView.delegate = self
        sceneView.presentScene(overlayScene)
        sceneView.session.delegate = self
        
        // Hook up status view controller callback.
        statusViewController.restartExperienceHandler = { [unowned self] in
            self.restartSession()
        }
        
        // Start the world map timer
        worldMapTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.sendWorldMap), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        print("default alignment", configuration.worldAlignment.rawValue)
        configuration.worldAlignment = .gravityAndHeading
        
        // Run the view's session
        sceneView.session.run(configuration)
        
        // Connect the socket to the correct place
        self.socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
        
        // Disconnect the socket
        self.socketClient.disconnect()
    }
    
    // MARK: - ARSessionDelegate
    
    // Pass camera frames received from ARKit to Vision (when not already processing one)
    /// - Tag: ConsumeARFrames
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        // Always send camera location, regardless of Vision processing state.
        let currentLocation = XYZTransform(frame.camera.transform)
        let currentOrientation = RPYTransform(frame.camera.eulerAngles)
        
        let t = frame.camera.transform
        let tm: [Float] = [
            t.columns.0[0], t.columns.1[0], t.columns.2[0], t.columns.3[0],
            t.columns.0[1], t.columns.1[1], t.columns.2[1], t.columns.3[1],
            t.columns.0[2], t.columns.1[2], t.columns.2[2], t.columns.3[2],
            t.columns.0[3], t.columns.1[3], t.columns.2[3], t.columns.3[3],
        ]
        
        let camTransform = CameraTransform(position: currentLocation, orientation: currentOrientation, transformMatrix: tm)
        self.sendEncodable(camTransform, destination: "/app/give/location")
        
        // Do not enqueue other buffers for processing while another Vision task is still running.
        // The camera stream has only a finite amount of buffers available; holding too many buffers for analysis would starve the camera.
        guard currentBuffer == nil, case .normal = frame.camera.trackingState else {
            return
        }
        
        // Retain the image buffer for Vision processing.
        self.analysedFrame = frame
        self.currentBuffer = frame.capturedImage
        classifyCurrentImage()
    }
    
    // MARK: - Timer
    var worldMapTimer: Timer!
    
    @objc
    func sendWorldMap() {
        sceneView.session.getCurrentWorldMap { worldMap, error in
            guard let worldMap = worldMap else {
                print("Couldn't get world map")
                return
            }
            self.sendEncodable(PointCloud(worldMap), destination: "/app/give/points")
        }
    }
    
    // MARK: - STOMP
    let socketClient = StompClientLib()
    let url = NSURL(string: "http://10.77.1.122:8080/thingfinder/websocket")!

    func stompClientDidConnect(client: StompClientLib!) {
        print("Socket is connected")
        // Stomp subscribe will be here!
        socketClient.subscribe(destination: "/topic/get/detections")
        
        // Server reset
        DispatchQueue.global(qos: .background).async {
            self.socketClient.sendMessage(message: "{}", toDestination: "/app/give/reset", withHeaders: ["content-type": "application/json"], withReceipt: nil)
        }
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("Socket disconnected")
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        if let bodyData: Data = jsonBody?.data(using: .utf8) {
            do {
                if destination == "/topic/get/detections" {
                    print("Detections received")
                    let receivedDetections = try jsonDecoder.decode(DetectionArray.self, from: bodyData)
                    
                    // Iterate over detections
                    DispatchQueue.main.async {
                        var sentIDs: [String] = []
                        
                        for detection in receivedDetections.detections {
                            sentIDs.append(detection.id!)
                            if let oldAnchor: ARAnchor = self.currentAnchors[detection.id!] {
                                // Existing anchor
                                if XYZTransform(oldAnchor.transform).roughlyEqual(detection.transform) {
                                    // Hasn't moved. Do nothing
                                } else {
                                    // Recreate the anchor with new position
                                    print("Recreating marker", detection.id)
                                    self.sceneView.session.remove(anchor: self.currentAnchors[detection.id!]!)
                                    self.currentAnchors.removeValue(forKey: detection.id!)
                                    
                                    self.placeLabelAt3DPoint(transform: detection.transform.toFloat4x4(), label: detection.className, id: detection.id!)
                                }
                            } else {
                                // New anchor
                                print("creating detection", detection.id)
                                self.placeLabelAt3DPoint(transform: detection.transform.toFloat4x4(), label: detection.className, id: detection.id!)
                            }
                        }
                        
                        // Remove detections that exist in anchors but weren't sent
                        for detectionID in self.currentAnchors.keys {
                            if !sentIDs.contains(detectionID) {
                                // Remove detection
                                print("removing detection", detectionID)
                                self.sceneView.session.remove(anchor: self.currentAnchors[detectionID]!)
                                self.currentAnchors.removeValue(forKey: detectionID)
                            }
                        }
                    }
                }
            } catch {
                print("Something went wrong decoding response")
                return
            }
        }
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error Send : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        print("Server ping")
    }
    
    // Serialization
    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()
    
    // Frame with multiple detections
    struct DetectionFrame : Encodable {
        let image: String  // base64 data
        let detections: [DetectedObject]
    }
    
    // Holds multiple detections. Received from server
    struct DetectionArray : Decodable {
        let detections: [DetectedObject]
    }
    
    // Detection input/output for server
    struct DetectedObject : Codable {
        let className: String
        let score: Float
        let transform: XYZTransform
        let boundingBox: BoundingBox?
        let id: String?
    }
    
    // Bounding box on image
    struct BoundingBox : Codable {
        let x: Float
        let y: Float
        let height: Float
        let width: Float
    }
    
    // x/y/z transform struct
    struct XYZTransform : Codable {
        let x: Float
        let y: Float
        let z: Float
        
        init(_ native: vector_float3) {
            self.x = native.x
            self.y = native.y
            self.z = native.z
        }
        
        init(_ native: simd_float4x4) {
            self.x = native.columns.3.x
            self.y = native.columns.3.y
            self.z = native.columns.3.z
        }
        
        func toFloat4x4() -> simd_float4x4 {
            var native = simd_float4x4(diagonal: float4([1.0, 1.0, 1.0, 1.0]))
            native.columns.3.x = self.x
            native.columns.3.y = self.y
            native.columns.3.z = self.z
            return native
        }
        
        func roughlyEqual(_ other: XYZTransform) -> Bool {
            return abs(self.x - other.x) < 0.01 && abs(self.y - other.y) < 0.01 && abs(self.z - other.z) < 0.01
        }
    }
    
    // yaw/pitch/roll transform
    struct RPYTransform : Encodable {
        let roll: Float
        let pitch: Float
        let yaw: Float
        
        init(_ native: simd_float3) {
            self.roll = native.x
            self.pitch = native.y
            self.yaw = native.z
        }
    }
    
    struct CameraTransform : Encodable {
        let position: XYZTransform
        let orientation: RPYTransform
        let transformMatrix: [Float]  // 4x4 matrix, row-major notation
    }
    
    struct PointCloud : Encodable {
        let center: XYZTransform
        let extent: XYZTransform
        let points: [XYZTransform]
        
        init(_ worldMap: ARWorldMap) {
            self.center = XYZTransform(worldMap.center)
            self.extent = XYZTransform(worldMap.extent)
            var points: [XYZTransform] = []
            for point in worldMap.rawFeaturePoints.points {
                points.append(XYZTransform(point))
            }
            self.points = points
        }
    }
    
    // MARK: - Vision classification
    
    // Crappy config
    // Tweak these values to get more or fewer predictions.
    let confidenceThreshold: Float = 0.8
    let iouThreshold: Float = 0.1
    let inputWidth = 416
    let inputHeight = 416
    let maxBoundingBoxes = 10
    
    // YOLO output
    struct Prediction {
        let classIndex: Int
        let score: Float
        let rect: CGRect
    }
    
    // Vision classification request and model
    /// - Tag: ClassificationRequest
    private lazy var classificationRequest: VNCoreMLRequest = {
        do {
            // Instantiate the model from its generated Swift class.
            let model = try VNCoreMLModel(for: YOLOv3().model)
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                self?.processClassifications(for: request, error: error)
            })
            
            // Crop input images to square area at center, matching the way the ML model was trained.
            request.imageCropAndScaleOption = .centerCrop
            
            // DON'T Use CPU for Vision processing to ensure that there are adequate GPU resources for rendering.
            request.usesCPUOnly = false
            
            return request
        } catch {
            fatalError("Failed to load Vision ML model: \(error)")
        }
    }()
    
    // The pixel buffer being held for analysis; used to serialize Vision requests.
    private var currentBuffer: CVPixelBuffer?
    
    // Queue for dispatching vision classification requests
    private let visionQueue = DispatchQueue(label: "com.example.apple-samplecode.ARKitVision.serialVisionQueue")
    
    // Run the Vision+ML classifier on the current image buffer.
    /// - Tag: ClassifyCurrentImage
    private func classifyCurrentImage() {
        // Most computer vision tasks are not rotation agnostic so it is important to pass in the orientation of the image with respect to device.
        let orientation = CGImagePropertyOrientation(UIDevice.current.orientation)
        
        let requestHandler = VNImageRequestHandler(cvPixelBuffer: currentBuffer!, orientation: orientation)
        visionQueue.async {
            do {
                // Release the pixel buffer when done, allowing the next buffer to be processed.
                defer { self.currentBuffer = nil }
                try requestHandler.perform([self.classificationRequest])
            } catch {
                print("Error: Vision request failed with error \"\(error)\"")
            }
        }
    }
    
    // Classification results
    private var analysedFrame: ARFrame!
    private var lastImage: UIImage!
    private var boundingBoxes: [Prediction] = []
    
    private var identifierString = ""
    private var confidence: VNConfidence = 0.0
    
    // Handle completion of the Vision request and choose results to display.
    /// - Tag: ProcessClassifications
    func processClassifications(for request: VNRequest, error: Error?) {
        guard let results = request.results else {
            print("Unable to classify image.\n\(error!.localizedDescription)")
            return
        }
        
        // The `results` will always be `VNCoreMLFeatureValueObservation`s, as specified by the Core ML model in this project.
        if let observations = results as? [VNCoreMLFeatureValueObservation] {
            let features = observations.map { $0.featureValue.multiArrayValue }
            boundingBoxes = computeBoundingBoxes(features: features as! [MLMultiArray])
        }
        
        DispatchQueue.main.async {
            let bounds = self.view.bounds
            DispatchQueue.global(qos: .background).async { [weak self] in
                self?.publishDetections(bounds: bounds)
//                self?.displayClassifierResults()
            }
        }
    }
    
    // Send some network stuff in background thread
    private func sendEncodable<T: Encodable>(_ encodable: T, destination: String) {
        let encoded: String!
        do {
            encoded = try String(data: self.jsonEncoder.encode(encodable), encoding: .utf8)!
        } catch {
            print("Could not encode info")
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            self.socketClient.sendMessage(message: encoded, toDestination: destination, withHeaders: ["content-type": "application/json"], withReceipt: nil)
        }
    }
    
    // Publish the classification results to server.
    private func publishDetections(bounds: CGRect) {
        var detections: [DetectedObject] = []
        
        for boundingBox in boundingBoxes {
            let scaledRect = scaleBoundingBox(predictionRect: boundingBox.rect)
            let rectCenter = CGPoint(x: scaledRect.midX, y: 1 - scaledRect.midY)
            
            // Perform hit test on the frame from which these bounding boxes were obtained.
            let hitTestResults = analysedFrame.hitTest(rectCenter, types: [.featurePoint, .estimatedHorizontalPlane])
//            let hitTestResults = sceneView.hitTest(rectCenter, types: [.featurePoint, .estimatedHorizontalPlane])
            
            if let result = hitTestResults.first {
                let transform = XYZTransform(result.worldTransform)
//                print("detected", labels[boundingBox.classIndex], "at", result.worldTransform, transform)
                let detection = DetectedObject(className: labels[boundingBox.classIndex], score: boundingBox.score, transform: transform, boundingBox: boundingBoxRelative(predictionRect: boundingBox.rect), id: nil)
                detections.append(detection)
            }
        }
            
        // Publish image and related detections
        let uiImage = UIImage(pixelBuffer: analysedFrame.capturedImage)!
        if let jpegData = uiImage.jpegData(compressionQuality: CGFloat(0.5)) as NSData? {
            let jpegStr = jpegData.base64EncodedString(options: .endLineWithCarriageReturn)
            let detectionsFrame = DetectionFrame(image: jpegStr, detections: detections)
            sendEncodable(detectionsFrame, destination: "/app/give/detection")
        } else {
            print("Couldn't send image: No data")
        }
    }
    
    // Show the classification results in the UI.
    private func displayClassifierResults() {
        for boundingBox in boundingBoxes {
            let identifierString = labels[boundingBox.classIndex]
//            let scaledRect = screenScaleBoundingBox(predictionRect: boundingBox.rect, viewRect: self.view.bounds)
//            let rectCenter = CGPoint(x: scaledRect.midX, y: scaledRect.midY)
            
            let scaledRect = scaleBoundingBox(predictionRect: boundingBox.rect)
            let rectCenter = CGPoint(x: scaledRect.midX, y: 1 - scaledRect.midY)
            
            placeLabelAtLocation(location: rectCenter, label: identifierString, id: UUID().uuidString)
            
            let message = String(format: "Detected \(identifierString) with %.2f", boundingBox.score * 100) + "% confidence"
            statusViewController.showMessage(message)
        }
    }
    
    // I borrowed this from https://github.com/hollance/YOLO-CoreML-MPSNNGraph
    private func computeBoundingBoxes(features: [MLMultiArray]) -> [Prediction] {
        assert(features[0].count == 255*13*13)
        assert(features[1].count == 255*26*26)
        assert(features[2].count == 255*52*52)
        
        var predictions = [Prediction]()
        
        let blockSize: Float = 32
        let boxesPerCell = 3
        let numClasses = 80
        
        // The 416x416 image is divided into a 13x13 grid. Each of these grid cells
        // will predict 5 bounding boxes (boxesPerCell). A bounding box consists of
        // five data items: x, y, width, height, and a confidence score. Each grid
        // cell also predicts which class each bounding box belongs to.
        //
        // The "features" array therefore contains (numClasses + 5)*boxesPerCell
        // values for each grid cell, i.e. 125 channels. The total features array
        // contains 255x13x13 elements.
        
        // NOTE: It turns out that accessing the elements in the multi-array as
        // `features[[channel, cy, cx] as [NSNumber]].floatValue` is kinda slow.
        // It's much faster to use direct memory access to the features.
        var gridHeight = [13, 26, 52]
        var gridWidth = [13, 26, 52]
        
        var featurePointer = UnsafeMutablePointer<Double>(OpaquePointer(features[0].dataPointer))
        var channelStride = features[0].strides[0].intValue
        var yStride = features[0].strides[1].intValue
        var xStride = features[0].strides[2].intValue
        
        func offset(_ channel: Int, _ x: Int, _ y: Int) -> Int {
            return channel*channelStride + y*yStride + x*xStride
        }
        
        for i in 0..<3 {
            featurePointer = UnsafeMutablePointer<Double>(OpaquePointer(features[i].dataPointer))
            channelStride = features[i].strides[0].intValue
            yStride = features[i].strides[1].intValue
            xStride = features[i].strides[2].intValue
            for cy in 0..<gridHeight[i] {
                for cx in 0..<gridWidth[i] {
                    for b in 0..<boxesPerCell {
                        // For the first bounding box (b=0) we have to read channels 0-24,
                        // for b=1 we have to read channels 25-49, and so on.
                        let channel = b*(numClasses + 5)
                        
                        // The fast way:
                        let tx = Float(featurePointer[offset(channel    , cx, cy)])
                        let ty = Float(featurePointer[offset(channel + 1, cx, cy)])
                        let tw = Float(featurePointer[offset(channel + 2, cx, cy)])
                        let th = Float(featurePointer[offset(channel + 3, cx, cy)])
                        let tc = Float(featurePointer[offset(channel + 4, cx, cy)])
                        
                        // The predicted tx and ty coordinates are relative to the location
                        // of the grid cell; we use the logistic sigmoid to constrain these
                        // coordinates to the range 0 - 1. Then we add the cell coordinates
                        // (0-12) and multiply by the number of pixels per grid cell (32).
                        // Now x and y represent center of the bounding box in the original
                        // 416x416 image space.
                        let x = (Float(cx) * blockSize + sigmoid(tx))
                        let y = (Float(cy) * blockSize + sigmoid(ty))
                        
                        // The size of the bounding box, tw and th, is predicted relative to
                        // the size of an "anchor" box. Here we also transform the width and
                        // height into the original 416x416 image space.
                        let w = exp(tw) * anchors[i][2*b    ]
                        let h = exp(th) * anchors[i][2*b + 1]
                        
                        // The confidence value for the bounding box is given by tc. We use
                        // the logistic sigmoid to turn this into a percentage.
                        let confidence = sigmoid(tc)
                        
                        // Gather the predicted classes for this anchor box and softmax them,
                        // so we can interpret these numbers as percentages.
                        var classes = [Float](repeating: 0, count: numClasses)
                        for c in 0..<numClasses {
                            // The slow way:
                            //classes[c] = features[[channel + 5 + c, cy, cx] as [NSNumber]].floatValue
                            
                            // The fast way:
                            classes[c] = Float(featurePointer[offset(channel + 5 + c, cx, cy)])
                        }
                        classes = softmax(classes)
                        
                        // Find the index of the class with the largest score.
                        let (detectedClass, bestClassScore) = classes.argmax()
                        
                        // Combine the confidence score for the bounding box, which tells us
                        // how likely it is that there is an object in this box (but not what
                        // kind of object it is), with the largest class prediction, which
                        // tells us what kind of object it detected (but not where).
                        let confidenceInClass = bestClassScore * confidence
                        
                        // Since we compute 13x13x3 = 507 bounding boxes, we only want to
                        // keep the ones whose combined score is over a certain threshold.
                        if confidenceInClass > confidenceThreshold {
                            let rect = CGRect(x: CGFloat(x - w/2), y: CGFloat(y - h/2),
                                              width: CGFloat(w), height: CGFloat(h))
                            
                            let prediction = Prediction(classIndex: detectedClass,
                                                        score: confidenceInClass,
                                                        rect: rect)
                            predictions.append(prediction)
                        }
                    }
                }
            }
        }
        
        // We already filtered out any bounding boxes that have very low scores,
        // but there still may be boxes that overlap too much with others. We'll
        // use "non-maximum suppression" to prune those duplicate bounding boxes.
        return nonMaxSuppression(boxes: predictions, limit: maxBoundingBoxes, threshold: iouThreshold)
    }
    
    private func boundingBoxRelative(predictionRect: CGRect) -> BoundingBox {
        return BoundingBox(x: Float(predictionRect.origin.x) / Float(inputWidth),
                           y: Float(predictionRect.origin.y) / Float(inputHeight),
                           height: Float(predictionRect.height) / Float(inputHeight),
                           width: Float(predictionRect.width) / Float(inputWidth))
    }
    
    private func scaleBoundingBox(predictionRect: CGRect) -> CGRect {
        let scaleX = CGFloat(1) / CGFloat(inputWidth)
        let scaleY = CGFloat(1) / CGFloat(inputHeight)
        
        var rect = predictionRect
        rect.origin.x *= scaleX
        rect.origin.y *= scaleY
        rect.origin.y += CGFloat(1) / CGFloat(6)
        rect.size.width *= scaleX
        rect.size.height *= scaleY
        
        var rectSwapped = CGRect()
        rectSwapped.origin.x = rect.origin.y
        rectSwapped.origin.y = rect.origin.x
        rectSwapped.size.width = rect.size.height
        rectSwapped.size.height = rect.size.width
        
        return rectSwapped
    }
    
    private func screenScaleBoundingBox(predictionRect: CGRect, viewRect: CGRect) -> CGRect {
        // The predicted bounding box is in the coordinate space of the input
        // image, which is a square image of 416x416 pixels. We want to show it
        // on the fullscreen video preview.
        let width = viewRect.width
        let height = viewRect.height
        let scaleX = width / CGFloat(inputWidth)
        let scaleY = height / CGFloat(inputHeight)
        
        // Translate and scale the rectangle to our own coordinate system.
        var rect = predictionRect
        rect.origin.x *= scaleX
        rect.origin.y *= scaleY
        rect.size.width *= scaleX
        rect.size.height *= scaleY
        
        return rect
    }
    
    // MARK: - Tap gesture handler & ARSKViewDelegate
    
    // Labels for classified objects by ARAnchor UUID
    private var currentAnchors = [String: ARAnchor]()
    private var anchorLabels = [UUID: String]()
    
    private func placeLabelAt3DPoint(transform: simd_float4x4, label: String, id: String) {
        // Add a new anchor at the tap location.
        let anchor = ARAnchor(transform: transform)
        currentAnchors[id] = anchor
        sceneView.session.add(anchor: anchor)
        
        // Track anchor ID to associate text with the anchor after ARKit creates a corresponding SKNode.
        anchorLabels[anchor.identifier] = label
    }
    
    // When the user taps, add an anchor associated with the current classification result.
    func placeLabelAtLocation(location: CGPoint, label: String, id: String) {
        let hitTestResults = analysedFrame.hitTest(location, types: [.featurePoint, .estimatedHorizontalPlane])
        if let result = hitTestResults.first {
            placeLabelAt3DPoint(transform: result.worldTransform, label: label, id: id)
        }
    }
    
    // When an anchor is added, provide a SpriteKit node for it and set its text to the classification label.
    /// - Tag: UpdateARContent
    func view(_ view: ARSKView, didAdd node: SKNode, for anchor: ARAnchor) {
        guard let labelText = anchorLabels[anchor.identifier] else {
            fatalError("missing expected associated label for anchor")
        }
        let label = TemplateLabelNode(text: labelText)
        node.addChild(label)
    }
    
    // MARK: - AR Session Handling
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        statusViewController.showTrackingQualityInfo(for: camera.trackingState, autoHide: true)
        
        switch camera.trackingState {
        case .notAvailable, .limited:
            statusViewController.escalateFeedback(for: camera.trackingState, inSeconds: 3.0)
        case .normal:
            statusViewController.cancelScheduledMessage(for: .trackingStateEscalation)
            // Unhide content after successful relocalization.
            setOverlaysHidden(false)
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        guard error is ARError else { return }
        
        let errorWithInfo = error as NSError
        let messages = [
            errorWithInfo.localizedDescription,
            errorWithInfo.localizedFailureReason,
            errorWithInfo.localizedRecoverySuggestion
        ]
        
        // Filter out optional error messages.
        let errorMessage = messages.compactMap({ $0 }).joined(separator: "\n")
        DispatchQueue.main.async {
            self.displayErrorMessage(title: "The AR session failed.", message: errorMessage)
        }
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        setOverlaysHidden(true)
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        /*
         Allow the session to attempt to resume after an interruption.
         This process may not succeed, so the app must be prepared
         to reset the session if the relocalizing status continues
         for a long time -- see `escalateFeedback` in `StatusViewController`.
         */
        return true
    }

    private func setOverlaysHidden(_ shouldHide: Bool) {
        sceneView.scene!.children.forEach { node in
            if shouldHide {
                // Hide overlay content immediately during relocalization.
                node.alpha = 0
            } else {
                // Fade overlay content in after relocalization succeeds.
                node.run(.fadeIn(withDuration: 0.5))
            }
        }
    }

    private func restartSession() {
        statusViewController.cancelAllScheduledMessages()
        statusViewController.showMessage("RESTARTING SESSION")
        
        currentAnchors = [String: ARAnchor]()
        anchorLabels = [UUID: String]()
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = .gravityAndHeading
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        
        // Server reset
        DispatchQueue.global(qos: .background).async {
            self.socketClient.sendMessage(message: "{}", toDestination: "/app/give/reset", withHeaders: ["content-type": "application/json"], withReceipt: nil)
        }
    }
    
    // MARK: - Error handling
    
    private func displayErrorMessage(title: String, message: String) {
        // Present an alert informing about the error that has occurred.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let restartAction = UIAlertAction(title: "Restart Session", style: .default) { _ in
            alertController.dismiss(animated: true, completion: nil)
            self.restartSession()
        }
        alertController.addAction(restartAction)
        present(alertController, animated: true, completion: nil)
    }
}
