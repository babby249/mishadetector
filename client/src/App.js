import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import SendIcon from '@material-ui/icons/Send';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import SockJsClient from "react-stomp";

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import Grow from '@material-ui/core/Grow';

const drawerWidth = 240;

let ObjectDetections = {"detections": []};
let objectMapping = {'aeroplane': 'Aeroplane', 'apple': 'Apple',
    'backpack': 'Backpack', 'banana': 'Banana', 'baseball bat': 'Baseball Bat',
    'baseball glove': 'Baseball Glove',
    'bear': 'Bear', 'bed': 'Bed', 'bench': 'Bench', 'bicycle': 'Bicycle',
    'bird': 'Bird', 'boat': 'Boat',
    'book': 'Book', 'bottle': 'Bottle',
    'bowl': 'Bowl', 'broccoli': 'Broccoli',
    'bus': 'Bus', 'cake': 'Cake',
    'car': 'Car', 'carrot': 'Carrot',
    'cat': 'Cat', 'cell phone': 'Cell Phone',
    'chair': 'Chair', 'clock': 'Clock',
    'cow': 'Cow', 'cup': 'Cup',
    'diningtable': 'Diningtable',
    'dog': 'Dog', 'donut': 'Donut',
    'elephant': 'Elephant',
    'fire hydrant': 'Fire Hydrant',
    'fork': 'Fork',
    'frisbee': 'Frisbee',
    'giraffe': 'Giraffe',
    'hair drier': 'Hair Drier',
    'handbag': 'Handbag',
    'horse': 'Horse',
    'hot dog': 'Hot Dog',
    'keyboard': 'Keyboard',
    'kite': 'Kite',
    'knife': 'Knife',
    'laptop': 'Laptop',
    'microwave': 'Microwave',
    'motorbike': 'Motorbike',
    'mouse': 'Mouse',
    'orange': 'Orange',
    'oven': 'Oven',
    'parking meter': 'Parking Meter',
    'person': 'Person',
    'pizza': 'Pizza',
    'pottedplant': 'Pottedplant',
    'refrigerator': 'Refrigerator',
    'remote': 'Remote',
    'sandwich': 'Sandwich',
    'scissors': 'Scissors',
    'sheep': 'Sheep',
    'sink': 'Sink',
    'skateboard': 'Skateboard',
    'skis': 'Skis',
    'snowboard': 'Snowboard',
    'sofa': 'Sofa',
    'spoon': 'Spoon',
    'sports ball': 'Sports Ball',
    'stop sign': 'Stop Sign',
    'suitcase': 'Suitcase',
    'surfboard': 'Surfboard',
    'teddy bear': 'Teddy Bear',
    'tennis racket': 'Tennis Racket',
    'tie': 'Tie',
    'toaster': 'Toaster',
    'toilet': 'Toilet',
    'toothbrush': 'Toothbrush',
    'traffic light': 'Traffic Light',
    'train': 'Train',
    'truck': 'Truck',
    'tvmonitor': 'Tvmonitor',
    'umbrella': 'Umbrella',
    'vase': 'Vase',
    'wine glass': 'Wine Glass',
    'zebra': 'Zebra'};

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    card: {
        maxWidth: '60vh',
    },
    media: {
        height: 0,
        // paddingTop: '56.25%', // 16:9
        paddingTop: '75%', // 4:3
    },
    appFrame: {
        // height: 430,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
});

class PermanentDrawer extends React.Component {

    constructor(){
        super();

        this.state = {
            mobileOpen: false,
            clicked: false,
            image:"",
        };

    }

    fetchImage=(uid)=>{
        console.log(uid);
        this.setState({imgUrl: "http://10.77.1.122:8080/api/detection/"+uid+"/image", clicked: true});
    };

    objectList=(dictionary)=>{
        // console.log(dictionary);
        let list = Object.keys(dictionary);

        let userList = list.map((item, iter)=>{
            let objectTitle = objectMapping[dictionary[item].className];
            return(
                <ListItem key={iter}>
                    <ListItemText  primary={objectTitle} />
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Find Item" onClick={()=>{
                            this.fetchImage(dictionary[item].id);
                            this.setState({title: objectTitle});
                        }}>
                            <SendIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            )
        });

        return(
            <List>
                {userList}
            </List>
        )
    };

    render() {
        const { classes } = this.props;

        const drawer = (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor={"left"}
            >
                <div className={classes.toolbar}/>
                <Divider />
                {this.state.reloading ? <div/> : this.objectList(ObjectDetections["detections"])}
            </Drawer>
        );


        return (
            <div className={classes.root}>

                <SockJsClient
                    url='http://10.77.1.122:8080/thingfinder'
                    topics={['/topic/get/points']}
                    // onConnect={console.log("Connection established!")}
                    // onDisconnect={console.log("Disconnected!")}
                    // onMessage={(msg) => { console.log("Points", msg); }}
                    onMessage={(msg) => {}}
                    // ref={ (client) => { this.clientRef = client }}
                    // debug={true}
                />

                <SockJsClient
                    url='http://10.77.1.122:8080/thingfinder'
                    topics={['/topic/get/detections']}
                    // onConnect={console.log("Connection established!")}
                    // onDisconnect={console.log("Disconnected!")}
                    onMessage={(msg) => {
                        if(msg!==ObjectDetections){
                            this.setState({reloading: true});
                            ObjectDetections = msg;
                            this.setState({reloading: false});
                        }
                        console.log("Detection", msg);

                    }}
                />

                <div className={classes.appFrame}>

                    <AppBar
                        position="absolute"
                        className={classNames(classes.appBar, classes[`appBar-left`])}
                    >
                        <Toolbar>
                            <Typography variant="title" color="inherit" noWrap>
                                ThingFinder
                            </Typography>
                        </Toolbar>
                    </AppBar>

                    {drawer}

                    <main className={classes.content}>
                        <div className={classes.toolbar} />

                        <Grow in={this.state.clicked}>
                            {this.state.clicked ?
                            <Card className={classes.card} style={{position: 'fixed', bottom: '2vh', right: '2vh', width: '100%'}}>

                                    <CardMedia
                                    className={classes.media}
                                    image={this.state.imgUrl}/>

                                <CardActions>
                                    <Typography variant="headline" component="h2">
                                        {this.state.title}
                                    </Typography>
                                    <Button size="small" color="primary" onClick={()=>{
                                        this.setState({clicked: false})
                                    }
                                    }>
                                        Close
                                    </Button>
                                </CardActions>
                            </Card>: <div/>
                            }
                        </Grow>

                    </main>
                </div>
            </div>
        );
    }
}

PermanentDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PermanentDrawer);