import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import MenuIcon from '@material-ui/icons/Menu';

import SendIcon from '@material-ui/icons/Send';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import SockJsClient from 'react-stomp';

import Bookmark from '@material-ui/icons/Bookmark';
import Search from '@material-ui/icons/Search';
import Timelapse from '@material-ui/icons/Timelapse';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import withMobileDialog from '@material-ui/core/withMobileDialog';

import compose from 'recompose/compose';

let SampleDetections = {
    "detections": [
        {
            "className": "person",
            "score": 0.8500955700874329,
            "transform": {"x": 0.464070200920105, "y": 0.015222504734992981, "z": 0.2883283495903015},
            "id": "1bc94abe-15e5-4e5b-a1b7-bb1db8ddab2a"
        }, {
            "className": "bottle",
            "score": 0.8182314038276672,
            "transform": {"x": 0.5199549198150635, "y": -0.0826156884431839, "z": 0.1849818229675293},
            "id": "da3b05a8-81c3-4888-80a7-d8268ba10481"
        }, {
            "className": "potato",
            "score": 0.9,
            "transform": {"x": 3.0, "y": 3.0, "z": 3.0},
            "id": "4dee14dd-ec14-4f77-920e-09e46f01a022"
        }, {
            "className": "cup",
            "score": 0.9935280084609985,
            "transform": {"x": 0.26743075251579285, "y": -0.17995208501815796, "z": -0.36569464206695557},
            "id": "78d0f190-369e-48c0-bdd1-a8e6438dccc6"
        }, {
            "className": "laptop",
            "score": 0.9998670816421509,
            "transform": {"x": 0.019527986645698547, "y": -0.04272603988647461, "z": -0.04055022820830345},
            "id": "f4c76502-7492-48a1-8bc5-2d5ea6d5733e"
        }]
};
let ObjectDetections = {"detections": []};

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100vh',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        position: 'absolute',
        zIndex: theme.zIndex.drawer + 1,
        // marginLeft: drawerWidth,
        // [theme.breakpoints.up('md')]: {
        //     width: `calc(100% - ${drawerWidth}px)`,
        // },
    },
    navIconHide: {
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            position: 'relative',
        },
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },

    spinner:{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

class ResponsiveDrawer extends React.Component {

    constructor(){
        super();

        this.state = {
            mobileOpen: false,
        };

    }

    handleDrawerToggle = () => this.setState({ mobileOpen: !this.state.mobileOpen });
    objectList=(dictionary)=>{
        console.log(dictionary);
        let list = Object.keys(dictionary);

        let userList = list.map((item)=>{
            return(
                <ListItem button>
                    <ListItemText primary={dictionary[item].className} />
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Find Item">
                            <SendIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            )
        });

        return(
            <List>
                {userList}
            </List>
        )
    };

    render() {
        const {classes, theme} = this.props;

        const drawer = (
            <div>
                <div className={classes.toolbar} />
                {this.objectList(ObjectDetections["detections"])}
            </div>
        );

        return (
            <div className={classes.root}>

                <SockJsClient
                    url='http://10.77.1.122:8080/thingfinder'
                    topics={['/topic/get/points']}
                    onConnect={console.log("Connection established!")}
                    onDisconnect={console.log("Disconnected!")}
                    onMessage={(msg) => { console.log("Points", msg); }}
                    // ref={ (client) => { this.clientRef = client }}
                    debug={true}
                />

                <SockJsClient
                    url='http://10.77.1.122:8080/thingfinder'
                    topics={['/topic/get/detections']}
                    onConnect={console.log("Connection established!")}
                    onDisconnect={console.log("Disconnected!")}
                    onMessage={(msg) => {
                        if(msg!==ObjectDetections){
                            ObjectDetections = msg;
                        }
                        console.log("Detection", msg);
                    }}
                    // ref={ (client) => { this.clientRef = client }}
                    debug={true}
                />

                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.navIconHide}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" noWrap>
                            ThingFinder
                        </Typography>
                    </Toolbar>
                </AppBar>

                <Hidden smUp>
                    <Drawer
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={this.state.mobileOpen}
                        onClose={this.handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        variant="permanent"
                        open
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>

            </div>
        );
    }
}

ResponsiveDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    fullScreen: PropTypes.bool.isRequired,
};

// export default compose(withStyles(styles, {withTheme: true}), withMobileDialog())(ResponsiveDrawer);
export default compose(withMobileDialog() ,withStyles(styles, {withTheme: true}))(ResponsiveDrawer);
// export default withStyles(styles, {withTheme: true})(ResponsiveDrawer);
// export default withMobileDialog()(ResponsiveDrawer);