if (!Detector.webgl) Detector.addGetWebGLMessage();

var renderer, scene, camera, stats;
var pointcloud;
var raycaster;
var mouse = new THREE.Vector2();
var clock;
var mouseDown;

var threshold = 0.1;
var pointSize = 0.1;

var pointCloudData = [
    {'x': -1.9290821552276611, 'y': -0.9342069625854492, 'z': 0.3406960964202881},
    {'x': -1.0081496238708496, 'y': -0.7359704375267029, 'z': 0.2515658438205719},
    {'x': -1.1153345108032227, 'y': -0.7688576579093933, 'z': 0.4665053188800812},
    {'x': -1.7884055376052856, 'y': -0.8773526549339294, 'z': 0.7690293788909912},
    {'x': -2.442955493927002, 'y': -0.1508752852678299, 'z': 1.5151479244232178},
    {'x': -0.8310530781745911, 'y': -0.7074514627456665, 'z': 0.6099452376365662},
    {'x': -1.6685409545898438, 'y': -1.2283748388290405, 'z': -0.22519217431545258},
    {'x': -1.4788645505905151, 'y': -1.033606767654419, 'z': -0.38139837980270386},
];

/////////////////
//User location dot

const sphereGeometry = new THREE.SphereGeometry(0.05, 32, 32);
const sphereMaterial = new THREE.MeshBasicMaterial({color: 0x1e88e5, shading: THREE.FlatShading});

let userLocationMarker = new THREE.Mesh(sphereGeometry, sphereMaterial);
let userLocationCoordinates = {'x': 0.1, 'y': 0.1, 'z': 0.1};
userLocationMarker.position.copy(userLocationCoordinates);
/////////////////

////////////////
//Uses POV Mesh

let mesh = null;
///////////////


/////////////////
//Object cloud
let objectCloud = null;

/////////////////////

var detectionHighlight = null;

function showCurrentInScene(){

    let boo = currentDetections.filter(a => a.id == currentView)[0]
    viewDetectionInScene(boo);

}



function viewDetectionInScene(boo){

    if(detectionHighlight) scene.remove(detectionHighlight);
    let detections = [boo];
    detectionHighlight = generatePointCloud(detections, new THREE.Color(1, 0, 0), size = 20, points=false);
    scene.add(detectionHighlight);
    $('#modal-close-button').trigger('click');

}


function showObjects(object) {

    // console.log("showObjects called");

    /*
    * {"className":"banana","score":0.9993171691894531,"transform":{"x":-0.19886088371276855,"y":-0.11280141770839691,"z":-0.4182125926017761},"id":"f61d14ad-0ba1-f364-fb4e-001c7c3207b5","boundingBox":{"x":0.3012826144695282,"y":0.1880592554807663,"height":0.24024929106235504,"width":0.32315143942832947}}
    * */


    if (objectCloud) {
        scene.remove(objectCloud);
    }
    const {detections} = object;
    // console.log(detections);
    objectCloud = generatePointCloud(detections, new THREE.Color(0, 1, 0), size = 4, points=false);


    scene.add(objectCloud);


}


init();
animate();

let currentDetections = [];

let currentView = null;

function showDetailedInformation(id, typeOfObject) {
    var modalInstance = M.Modal.getInstance(document.getElementById("modalDetailedInfo"));

    let booImg = $('#imageBig')
    booImg.transition({ opacity: 0}, 0)

    document.getElementById("imageBig").src = "http://10.77.1.122:8080/api/detection/" + id + "/image";
    document.getElementById("itemTitle").innerText = typeOfObject;

    booImg.one("load", function() {
      booImg.transition({ opacity: 1}, 200, 'ease')
    }).each(function() {
      // if(this.complete) $(this).load();
    });

    currentView = id;

    modalInstance.open();

}

function showListOfObjects(detections) {

    function getEntry(entry) {

        const {className, id} = entry;
        return `<li data-text="${capitalizeFirstLetter(className)}" class="collection-item black-text" onclick="showDetailedInformation('${id}', '${capitalizeFirstLetter(className)}')" >
                    <div>
                        ${capitalizeFirstLetter(className)}
                        <a href="#!" class="secondary-content">
                            <i class="material-icons">send</i>
                        </a>
                    </div>
                </li>`;
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }


    //Get the element
    const element = document.getElementById("listOfObjects");

    let innerElements = "";

    for (let i = 0; i < detections.length; i++) {
        const entry = detections[i];
        innerElements += getEntry(entry);
    }

    element.innerHTML = innerElements;
    search();

    //Create the list elements
    //Add them in


}

let counterGlobal = 0;

function updateUserPosition(position) {

    // if (userLocationMarker) {
    //     scene.remove(userLocationMarker);
    //     userLocationCoordinates = position;
    //     userLocationMarker.position.copy(userLocationCoordinates);
    //     scene.add(userLocationMarker);
    // }

}

function updatePOVProjection(matrixTransformArray) {
    function getMesh(arrayMatrix) {
        let geometry, material;
        geometry = new THREE.Geometry();

        geometry.vertices = [
            new THREE.Vector3(0, 0, 0),
            new THREE.Vector3(0, 100, 0),
            new THREE.Vector3(100, 100, 0),
            new THREE.Vector3(100, 0, 0),
            new THREE.Vector3(50, 50, 100)
        ];

        geometry.faces = [
            new THREE.Face3(0, 1, 2),
            new THREE.Face3(0, 2, 3),
            new THREE.Face3(1, 0, 4),
            new THREE.Face3(2, 1, 4),
            new THREE.Face3(3, 2, 4),
            new THREE.Face3(0, 3, 4)
        ];

        geometry.applyMatrix(new THREE.Matrix4().makeTranslation(-50, -50, -100));

        let scale = 0.015;
        let transformation = new THREE.Matrix4().makeScale(scale, 3 / 4 * scale, 2 * scale);

        const m = new THREE.Matrix4();


        if(arrayMatrix) m.set(...arrayMatrix);


        geometry.applyMatrix(transformation);
        geometry.applyMatrix(m);

        let deepblue = 0x1e88e5;
        let lightblue = 0x1565C0;


        let colours = [deepblue, deepblue, lightblue];

        for (let i = 0; i < 4; i++) {

            var faceIndex = i + 2;

            // the face's indices are labeled with these characters
            var faceIndices = ['a', 'b', 'c', 'd'];

            var face = geometry.faces[faceIndex];

            // determine if face is a tri or a quad
            var numberOfSides = (face instanceof THREE.Face3) ? 3 : 4;

            // assign color to each vertex of current face
            for (var j = 0; j < numberOfSides; j++) {
                var vertexIndex = face[faceIndices[j]];
                // initialize color variable
                var color = new THREE.Color(colours[j]);
                // color.setRGB( Math.random(), 0, 0 );
                face.vertexColors[j] = color;
            }

            face.opacity = 0;

        }


        let colours2 = [0xfafafa, 0xf5f5f5, 0xeeeeee];

        for (let i = 0; i < 2; i++) {

            var faceIndex = i;

            // the face's indices are labeled with these characters
            var faceIndices = ['a', 'b', 'c', 'd'];

            var face = geometry.faces[faceIndex];

            // determine if face is a tri or a quad
            var numberOfSides = (face instanceof THREE.Face3) ? 3 : 4;

            // assign color to each vertex of current face
            for (var j = 0; j < numberOfSides; j++) {
                // console.log(numberOfSides)
                var vertexIndex = face[faceIndices[j]];
                // initialize color variable

                var color = new THREE.Color(lightblue);
                // var color = new THREE.Color( 0x0288d1 );

                // color.setRGB( Math.random(), 0, 0 );
                face.vertexColors[j] = color;
            }

            face.opacity = 0;

        }


        material = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            shading: THREE.FlatShading,
            vertexColors: THREE.VertexColors
        });
        material.transparent = true;
        material.opacity = 0.7;


        mesh = new THREE.Mesh(geometry, material);


        const geometry2 = new THREE.EdgesGeometry(geometry); // or WireframeGeometry
        const material2 = new THREE.LineBasicMaterial({color: 'white', linewidth: 10}); // linewidth doesn't seem to do anything
        const edges2 = new THREE.LineSegments(geometry2, material2);
        mesh.add(edges2);
        // add wireframe as a child of the parent mesh

        return mesh;
    }

    if (mesh) {
        scene.remove(mesh);
    }


    mesh = getMesh(matrixTransformArray);
    scene.add(mesh)
}

function connect() {
    console.log("Connect called");
    var socket = new SockJS('http://10.77.1.122:8080/thingfinder');
    stompClient = Stomp.over(socket);
    stompClient.debug = null;
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/get/points', function (points) {
            // console.log('Points Subscription');
            points = JSON.parse(points.body);

            //TODO change the pointCloudData variable for the points and rerender.
            pointCloudData = points.points;

            //TODO call the showPointCloudData() function
            showPointCloudData(pointCloudData);
        });

        stompClient.subscribe('/topic/get/detections', function (detection) {
            detection = JSON.parse(detection.body);
            //This is all the objects detected
            //Show the object green cloud
            //Update the list on the left side.

            counterGlobal++;
            if (counterGlobal % 3 === 0 || counterGlobal === 1) {
                currentDetections = detection.detections;
                showListOfObjects(detection.detections);
                showObjects(detection);
            }


            //TODO showPointObjectData green cloud
            //TODO update list on the side.
        });

        stompClient.subscribe('/topic/get/location', function (location) {
            location = JSON.parse(location.body);

            //.position to place the user location coordinates red dot.
            updateUserPosition(location.position);
            //.transformMatrix to calculate the projection
            //Redo the projection on the render.
            // console.log(location);

            const {transformMatrix} = location;
            updatePOVProjection(transformMatrix);
        });
    });
}

connect();


function generatePointCloud(vertices, color, size = pointSize, points=true) {

    function getMinMax(vertices) {
        var lowest = Number.POSITIVE_INFINITY;
        var highest = Number.NEGATIVE_INFINITY;
        var tmp;
        for (var i = vertices.length - 1; i >= 0; i--) {
            tmp = countDistanceToCamera(vertices[i]);
            if (tmp < lowest) lowest = tmp;
            if (tmp > highest) highest = tmp;
        }
        // console.log(highest, lowest);
        return [lowest, highest];
    }

    function countDistanceToCamera(obj) {
        var x = camera.position.x;
        var y = camera.position.y;
        var z = camera.position.z;
        var distance = Math.sqrt((obj.x - x) * (obj.x - x) + (obj.y - y) * (obj.y - y) + (obj.z - z) * (obj.z - z));
        return distance;
    }


    var geometry = new THREE.Geometry();
    var colors = [];
    var k = 0;


    var minMax = getMinMax(vertices);
    var min = minMax[0];
    var max = minMax[1];


    for (var i = 0, l = vertices.length; i < l; i++) {


        vertex = vertices[i];

        if(!points){
            vertex = vertex.transform;
        }

        var v = new THREE.Vector3(vertex.x, vertex.y, vertex.z);
        geometry.vertices.push(v);

        ///////////////////////////

        let distanceFromCameraToVertex = countDistanceToCamera(vertex);
        distanceFromCameraToVertex = distanceFromCameraToVertex < 0 ? distanceFromCameraToVertex * -1 : distanceFromCameraToVertex;

        var intensity = (max - distanceFromCameraToVertex) / max * 3;

        if (!points){
            intensity = 2;
        }

        //////////////////////////


        colors[k] = (color.clone().multiplyScalar(intensity));

        k++;
    }

    geometry.colors = colors;
    geometry.computeBoundingBox();

    var material = new THREE.PointsMaterial({size: pointSize, vertexColors: THREE.VertexColors});
    var pointcloud = new THREE.Points(geometry, material);

    return pointcloud;

}

function init() {

    var container = document.getElementById('container');

    scene = new THREE.Scene();

    // var geometryPlane = new THREE.PlaneGeometry( 2000, 2000, 8, 8 );
    // var materialPlane = new THREE.MeshBasicMaterial( {color: 0xffffff, side: THREE.DoubleSide} );

    // var plane = new THREE.Mesh( geometryPlane, materialPlane );
    // plane.rotateX( - Math.PI / 2);
    // plane.position.y = -1;

    // scene.add( plane );



    clock = new THREE.Clock();

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.y = 25;
    camera.position.z = 37;


    renderer = new THREE.WebGLRenderer({preserveDrawingBuffer: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    //

    raycaster = new THREE.Raycaster();
    raycaster.params.Points.threshold = threshold;


    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.enabled = true;

    //

    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('mouseup', onDocumentMouseUp, false);


    // scene.add(userLocationMarker);


    /*var grid = new THREE.GridHelper(200, 20);
    grid.setColors(0xffffff, 0xffffff);
    scene.add(grid);*/


    //
    showPointCloudData(pointCloudData);

    $(function(){

        let input = $('#first_name');
        input.keyup(function(){

            search();

        });
    })

}

function search(){

    let input = $('#first_name');
    let val = input.val().toLowerCase();

    let objects = $('#listOfObjects');
    objects.children().each(function(i) {
        let me = $(this); 
        let text = me.attr('data-text').toLowerCase();
        if(val !== '' && text.indexOf(val) === -1){
            me.addClass('hidden')
        } else {
            me.removeClass('hidden')
        }
    });

}

function onDocumentMouseMove(event) {

    event.preventDefault();

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

}

function onDocumentMouseUp(event) {

    event.preventDefault();
    mouseDown = false;

}

function onDocumentMouseDown(event) {

    event.preventDefault();
    mouseDown = true;

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function animate() {

    requestAnimationFrame(animate);    

    if(mesh && objectCloud && pointcloud && userLocationMarker){
        mesh.rotation.y = Date.now() * 0.0001;  
        objectCloud.rotation.y = Date.now() * 0.0001;  
        pointcloud.rotation.y = Date.now() * 0.0001; 
        if(detectionHighlight) detectionHighlight.rotation.y = Date.now() * 0.0001; 
        // userLocationMarker.rotation.y = Date.now() * 0.0001; 
    }

    render();

}


function render() {

    raycaster.setFromCamera(mouse, camera);
    renderer.render(scene, camera);

}


function showPointCloudData(pointCloudData) {


    var rotation = 0;

    if (pointcloud !== undefined) {
        scene.remove(pointcloud);
        rotation = pointcloud.rotation.y;
    }

    pointcloud = generatePointCloudForCluster(pointCloudData);
    pointcloud.rotation.y = rotation;
    scene.add(pointcloud);
}

function generatePointCloudForCluster(pointCloudData) {
    return generatePointCloud(pointCloudData, new THREE.Color(1, 1, 1));
}


// https://stackoverflow.com/a/15327425/4855984
String.prototype.format = function () {
    var a = this, b;
    for (b in arguments) {
        a = a.replace(/%[a-z]/, arguments[b]);
    }
    return a; // Make chainable
};