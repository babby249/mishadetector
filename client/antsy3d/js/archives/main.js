if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

var renderer, scene, camera, stats;
var pointcloud;
var raycaster;
var mouse = new THREE.Vector2();
var intersection = null;
var spheres = [];
var pointsWithSpheres = new Set();
var clock;
var mouseDown;
var highlightMode = false;

var threshold = 0.1;
var pointSize = 0.05;

init();
animate();

function getMesh(arrayMatrix) {
    var geometry, material;
    geometry = new THREE.Geometry();

    geometry.vertices = [
        new THREE.Vector3( 0, 0, 0 ),
        new THREE.Vector3( 0, 100, 0 ),
        new THREE.Vector3( 100, 100, 0 ),
        new THREE.Vector3( 100, 0, 0 ),
        new THREE.Vector3( 50, 50, 100 )
    ];

    geometry.faces = [
        new THREE.Face3( 0, 1, 2 ),
        new THREE.Face3( 0, 2, 3 ),
        new THREE.Face3( 1, 0, 4 ),
        new THREE.Face3( 2, 1, 4 ),
        new THREE.Face3( 3, 2, 4 ),
        new THREE.Face3( 0, 3, 4 )
    ];

    geometry.applyMatrix( new THREE.Matrix4().makeTranslation( -50, -50, -100 ) );

    var transformation = new THREE.Matrix4().makeScale( 0.07, 9/16 * 0.07, 2 * 0.07 );

    var m = new THREE.Matrix4();


    m.set(...arrayMatrix);


    geometry.applyMatrix( transformation );
    geometry.applyMatrix(m);

    let deepblue = 0xff6f00;
    let lightblue = 0xffd54f;



    let colours = [ deepblue, deepblue, lightblue ];

    for (var i = 0; i < 4; i++) {

        var faceIndex = i + 2;

        // the face's indices are labeled with these characters
        var faceIndices = ['a', 'b', 'c', 'd'];

        var face = geometry.faces[ faceIndex ];

        // determine if face is a tri or a quad
        var numberOfSides = ( face instanceof THREE.Face3 ) ? 3 : 4;

        // assign color to each vertex of current face
        for( var j = 0; j < numberOfSides; j++ ) {
            console.log(numberOfSides);
            var vertexIndex = face[ faceIndices[ j ] ];
            // initialize color variable
            var color = new THREE.Color( colours[j] );
            // color.setRGB( Math.random(), 0, 0 );
            face.vertexColors[ j ] = color;
        }

        face.opacity = 0;

    }


    let colours2 = [ 0xfafafa, 0xf5f5f5, 0xeeeeee ];

    for (var i = 0; i < 2; i++) {

        var faceIndex = i;

        // the face's indices are labeled with these characters
        var faceIndices = ['a', 'b', 'c', 'd'];

        var face = geometry.faces[ faceIndex ];

        // determine if face is a tri or a quad
        var numberOfSides = ( face instanceof THREE.Face3 ) ? 3 : 4;

        // assign color to each vertex of current face
        for( var j = 0; j < numberOfSides; j++ ) {
            console.log(numberOfSides)
            var vertexIndex = face[ faceIndices[ j ] ];
            // initialize color variable

            var color = new THREE.Color( lightblue );
            // var color = new THREE.Color( 0x0288d1 );

            // color.setRGB( Math.random(), 0, 0 );
            face.vertexColors[ j ] = color;
        }

        face.opacity = 0;

    }



    // mesh = new THREE.Mesh(geometry, material);

    // geometry = new THREE.CylinderGeometry( 1, 10*3, 10*3, 4 );

    material = new THREE.MeshBasicMaterial(  { color: 0xffffff, shading: THREE.FlatShading, vertexColors: THREE.VertexColors } );

    material.transparent = true;
    material.opacity  = 0.7;

    // material = new THREE.MeshNormalMaterial({wireframe:true} )
    // material = new THREE.MeshBasicMaterial( {color: 0xffff00 , wireframe:true} );

    mesh = new THREE.Mesh( geometry, material );



    var geometry2 = new THREE.EdgesGeometry( geometry ); // or WireframeGeometry
    var material2 = new THREE.LineBasicMaterial( { color: 'white', linewidth: 10 } ); // linewidth doesn't seem to do anything
    var edges2 = new THREE.LineSegments( geometry2, material2 );
    mesh.add( edges2 ); // add wireframe as a child of the parent mesh

    return mesh;
}


function countDinstanceToCamera(obj){
    var x = camera.position.x;
    var y = camera.position.y;
    var z = camera.position.z;
    var distance = Math.sqrt((obj.x - x) * (obj.x - x) + (obj.y - y) * (obj.y - y) + (obj.z - z) * (obj.z - z));
    return distance;
}

var current_orientation = null;
function connect() {
    var socket = new SockJS('http://10.77.1.122:8080/thingfinder');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {

        console.log('Connected: ' + frame);

        console.log('Subscribing to /get/points');

        stompClient.subscribe('/topic/get/points', function (points) {
            console.log('Points!');
            // console.log(points.body);

           points = JSON.parse(points.body);

            pointCloudData['frog'].vertices = points.points;

            show(document.getElementById('obj0'), Object.keys(pointCloudData)[0]);


        });

        stompClient.subscribe('/topic/get/detections', function (greeting) {
            greeting = JSON.parse(greeting.body);
            console.log("Detection is: ");
            console.log(greeting);
            showObjects(greeting);
            // user_current_location = greeting.position;

        });

        stompClient.subscribe('/topic/get/location', function (greeting) {
            greeting = JSON.parse(greeting.body);
            console.log(greeting);
            userLocationCoordinates = greeting.position;

            current_orientation = greeting.transformMatrix;
            console.log(current_orientation);
        });
    });
}

// Should be in init?
var sphereGeometry, sphereMaterial;


function generatePointCloudObjects( vertices, color ) {

    var geometry = new THREE.Geometry();

    var colors = [];

    var k = 0;

    // console.log(vertices);

    /*var min_v_max = get_min_max(vertices);
    var min = min_v_max[0];
    var max = min_v_max[1];*/

    for ( var i = 0, l = vertices.length; i < l; i ++ ) {

        vertex = vertices[ i ]["transform"];

        var v = new THREE.Vector3( vertex.x, vertex.y, vertex.z );
        geometry.vertices.push( v );

        // var intensity = ( vertex.y + 0.1 ) * 7;

        // let dis = countDinstanceToCamera(vertex);



        // var intensity = ( vertex.y + 0.1 ) * 5;

        // dis = dis < 0 ? dis * -1 : dis;
        // console.log(dis);

        var intensity = 0;
        // ((dis - (max - min))/max);
        // intensity = (max - dis) / max;
        intensity = 1;

        // var intensity = (0.1) * 7;
        colors[ k ] = ( color.clone().multiplyScalar( intensity ) );

        k++;
    }

    geometry.colors = colors;
    geometry.computeBoundingBox();

    var material = new THREE.PointsMaterial( { size: pointSize, vertexColors: THREE.VertexColors } );
    var pointcloud = new THREE.Points( geometry, material );

    return pointcloud;

}

function generatePointCloudGeometry( vertices, color ){

    var geometry = new THREE.BufferGeometry();

    var positions = new Float32Array( vertices.length*3 );
    var colors = new Float32Array( vertices.length*3 );

    var k = 0;

    for ( var i = 0, l = vertices.length; i < l; i ++ ) {

        vertex = vertices[ i ];
        positions[ 3 * k ] = vertex.x;
        positions[ 3 * k + 1 ] = vertex.y;
        positions[ 3 * k + 2 ] = vertex.z;

        let dis = countDinstanceToCamera(vertex);

        // var intensity = ( vertex.y + 0.1 ) * 5;

        dis = dis < 0 ? dis * -1 : dis;

        var intensity = (dis) * 5;
        colors[ 3 * k ] = color.r * intensity;
        colors[ 3 * k + 1 ] = color.g * intensity;
        colors[ 3 * k + 2 ] = color.b * intensity;

        k++;
    }

    geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
    geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );
    geometry.computeBoundingBox();

    return geometry;

}

function get_min_max(vertices) {
    var lowest = Number.POSITIVE_INFINITY;
    var highest = Number.NEGATIVE_INFINITY;
    var tmp;
    for (var i=vertices.length-1; i>=0; i--) {
        tmp = countDinstanceToCamera(vertices[i]);
        if (tmp < lowest) lowest = tmp;
        if (tmp > highest) highest = tmp;
    }
    console.log(highest, lowest);
    return [lowest, highest];
}


function generatePointCloud( vertices, color ) {

    var geometry = new THREE.Geometry();

    var colors = [];

    var k = 0;

    // console.log(vertices);

    var min_v_max = get_min_max(vertices);
    var min = min_v_max[0];
    var max = min_v_max[1];

    for ( var i = 0, l = vertices.length; i < l; i ++ ) {

        vertex = vertices[ i ];
        var v = new THREE.Vector3( vertex.x, vertex.y, vertex.z );
        geometry.vertices.push( v );

        // var intensity = ( vertex.y + 0.1 ) * 7;

        let dis = countDinstanceToCamera(vertex);



        // var intensity = ( vertex.y + 0.1 ) * 5;

        dis = dis < 0 ? dis * -1 : dis;
        // console.log(dis);

        var intensity = 0;
            // ((dis - (max - min))/max);
        intensity = (max - dis) / max;

        // var intensity = (0.1) * 7;
        colors[ k ] = ( color.clone().multiplyScalar( intensity ) );

        k++;
    }

    geometry.colors = colors;
    geometry.computeBoundingBox();

    var material = new THREE.PointsMaterial( { size: pointSize, vertexColors: THREE.VertexColors } );
    var pointcloud = new THREE.Points( geometry, material );

    return pointcloud;

}

var camera2 = null;
var userLocationMarker;
var userLocationCoordinates = {'x': 0.1, 'y': 0.1, 'z': 0.1};
var mesh;
function init() {

    connect();

    var container = document.getElementById( 'container' );

    scene = new THREE.Scene();

    clock = new THREE.Clock();

    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.y = 25;
    camera.position.z = 37;

    camera2 = camera.clone();


    //

    sphereGeometry = new THREE.SphereGeometry( 0.05, 32, 32 );
    sphereMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, shading: THREE.FlatShading } );


    // mesh = getMesh();



    userLocationMarker = new THREE.Mesh( sphereGeometry, sphereMaterial );
    userLocationCoordinates = {'x': 0.1, 'y': 0.1, 'z': 0.1};
    userLocationMarker.position.copy( userLocationCoordinates );
    scene.add(userLocationMarker);




   /* current_position_sphere = new THREE.Geometry();

    var star = new THREE.Vector3();
    star.x = 0;
    star.y = 0;
    star.z = 0;

    current_position_sphere.vertices.push( star );

    var starsMaterial = new THREE.PointsMaterial( { color: 0xFF0000 } );

    var starField = new THREE.Points( current_position_sphere, starsMaterial );

    scene.add( starField );*/

    //var starsGeometry = new THREE.Geometry();

    //

    var grid = new THREE.GridHelper( 200, 20 );
    grid.setColors( 0xffffff, 0xffffff );
    // scene.add( grid );




    //

    renderer = new THREE.WebGLRenderer({preserveDrawingBuffer: true});
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    container.appendChild( renderer.domElement );

    //

    raycaster = new THREE.Raycaster();
    raycaster.params.Points.threshold = threshold;

    //

    stats = new Stats();
    container.appendChild( stats.dom );

    //

    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.enabled = false;



    //

    window.addEventListener( 'resize', onWindowResize, false );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener( 'mouseup', onDocumentMouseUp, false );
    document.getElementById( 'save' ).addEventListener( 'click', save, false );
    document.getElementById( 'export' ).addEventListener( 'click', save_image, false );
    document.getElementById( 'move' ).addEventListener( 'click', moveMode, false );
    document.getElementById( 'label' ).addEventListener( 'click', labelMode, false );

    //

    initNav();
    show(document.getElementById('obj0'), Object.keys(pointCloudData)[0]);

}

function onDocumentMouseMove( event ) {

    event.preventDefault();

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

}

function onDocumentMouseUp( event ) {

    event.preventDefault();
    mouseDown = false;

}

function onDocumentMouseDown( event ) {

    event.preventDefault();
    mouseDown = true;

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    console.log("Camera change");

    renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

    requestAnimationFrame( animate );

    render();
    stats.update();

}

var toggle = 0;

var starsGeometry = null;

var zzx = 0;

let ixxx = 0;

var current_position_sphere;

function render() {

    raycaster.setFromCamera( mouse, camera );


    //console.log("render called");


    /*if (zzx % 180 === 0){
        showPointCloudData(document.getElementById('obj0'), Object.keys(pointCloudData)[0]);
    }

    zzx++;*/

    scene.remove(userLocationMarker);

    userLocationMarker.position.copy( userLocationCoordinates );
    scene.add(userLocationMarker);

    if (current_orientation) {

        scene.remove(mesh);

        mesh = getMesh(current_orientation);

        // mesh.rotation.x = current_orientation.yaw;
        // mesh.rotation.y = current_orientation.roll;
        // mesh.rotation.z = ixxx / 360;

        // ixxx++;

       /* let debug = JSON.stringify(current_orientation);

        debug = "Yaw: " + current_orientation.yaw.toFixed(2) + "\n Roll: " + current_orientation.roll.toFixed(2) + "\n" +
            "Pitch: " + current_orientation.pitch.toFixed(2);

            document.getElementById("debug_tag").innerHTML = debug;*/

        // mesh.position.copy( userLocationCoordinates );


        scene.add(mesh);

    }




    var intersections = raycaster.intersectObjects( [pointcloud] );
    intersection = ( intersections.length ) > 0 ? intersections[ 0 ] : null;

    if ( toggle > 0.005 && intersection !== null && !(
            pointsWithSpheres.has(intersection.index)) && mouseDown
            && !controls.enabled) {

        var point = pointcloud.geometry.vertices[intersection.index];
        var sphere = new THREE.Mesh( sphereGeometry, sphereMaterial );
        sphere.position.copy( point );
        scene.add(sphere);
        spheres.push(sphere);
        pointsWithSpheres.add(intersection.index);

        toggle = 0;
    }


    // current_position_sphere.position.copy( point );
    // scene.add(sphere);




    if (!starsGeometry) {
        starsGeometry = new THREE.Geometry();

        for (var i = 0; i < 10000; i++) {

            var star = new THREE.Vector3();
            star.x = THREE.Math.randFloatSpread(2000);
            star.y = THREE.Math.randFloatSpread(2000);
            star.z = THREE.Math.randFloatSpread(2000);

            starsGeometry.vertices.push(star);

        }

        var starsMaterial = new THREE.PointsMaterial({color: 0x888888});

        var starField = new THREE.Points(starsGeometry, starsMaterial);

        //scene.add( starField );
    }


    toggle += clock.getDelta();

    renderer.render( scene, camera );

}

// Navigation Bar

function initNav() {
    navigation = document.getElementById('navigation');
    var i = 0;
    for (key in pointCloudData) {
        var li = document.createElement('li');
        var element = document.createElement('a');
        li.appendChild(element);
        element.innerHTML = i + 1;
        element.id = 'obj' + i;
        wrapper = function(key) { return function() { show(this, key); }}
        element.onclick = wrapper(key);
        navigation.appendChild(li);
        i += 1;
    }
}

function clearNav() {
    for (var i = 0; i < Object.keys(pointCloudData).length; i++) {
        document.getElementById('obj' + i).className = '';
    }
}

function show(button, obj_name) {
    clearNav();
    button.className += "selected";
    var rotation = 0;

    if (pointcloud !== undefined) {
        scene.remove(pointcloud);
        rotation = pointcloud.rotation.y;
    }

    pointcloud = generatePointCloudForCluster( obj_name );
    pointcloud.rotation.y = rotation;
    scene.add( pointcloud );
}

var objectCloud = null;

function showObjects(object) {


    console.log("Trying to showPointCloudData objects of length: " + object.detections.length);





        objectCloud = generatePointCloudObjects(object.detections, new THREE.Color(0, 1, 0));

        // console.log("LOLOL");
        // console.log(objectCloud);
    objectCloud.rotation.y = 0;

        scene.add(objectCloud);


}

function generatePointCloudForCluster(obj_name) {
    return generatePointCloud(pointCloudData[obj_name]['vertices'], new THREE.Color( 1,1,1 ));
}

function moveMode( event ) {
    event.preventDefault();
    controls.enabled = true;
    document.getElementById( 'label' ).className = "";
    document.getElementById( 'move' ).className = "selected";
}

function labelMode( event ) {
    event.preventDefault();
    controls.enabled = false;
    document.getElementById( 'label' ).className = "selected";
    document.getElementById( 'move' ).className = "";
}

function save() {
    textContents = [];
    var numHighlighted = 0;
    for (var i=0;i<pointcloud.geometry.vertices.length;i++) {
        var point = pointcloud.geometry.vertices[i];
        var highlighted = pointsWithSpheres.has(i) ? 1 : 0;
        numHighlighted += highlighted;
        string = "%f,%f,%f,%d\n".format(point.x, point.y, point.z, highlighted);
        textContents.push(string);
    }
    console.log('Number of highlighted points: ' + numHighlighted.toString());
    var blob = new Blob(textContents, {type: "text/plain;charset=utf-8"});
    saveAs(blob, "labelled.csv");
}

function save_image() {
    renderer.domElement.toBlob(function (blob) {
        saveAs(blob, "image.png");
    });
}

// https://stackoverflow.com/a/15327425/4855984
String.prototype.format = function(){
    var a = this, b;
    for(b in arguments){
        a = a.replace(/%[a-z]/,arguments[b]);
    }
    return a; // Make chainable
};
